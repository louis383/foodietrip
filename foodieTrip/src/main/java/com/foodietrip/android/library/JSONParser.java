package com.foodietrip.android.library;

import android.content.Context;
import android.util.Log;

import com.foodietrip.android.R;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.PasswordAuthentication;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public class JSONParser {
    Context context;
    private JSONObject jsonObject;
    private NetworkState netState;
    private int timeOutConnection = 1000 * 15;
    private String hostUrl = "http://proposal.twbbs.org/api/index.php";
    private int httpResponseCode = 0;

    public JSONParser(final Context _context) {
        this.context = _context;
        this.netState = new NetworkState(_context);
        // 授權
        Authenticator.setDefault(new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                String username = _context.getResources().getString(R.string.nuclear_login);
                String password = _context.getResources().getString(R.string.nuclear_pw);
                return new PasswordAuthentication(username, password.toCharArray());
            }
        });
    }

    // 預設是 GET, 接收 JSON
    public JSONObject getJsonData(String action, String method) {
        // 檢查網路的狀態
        if ( !netState.checkInternet() ) return null;    // offline
        try {
            URL url = new URL( this.hostUrl +"/" +action );
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setConnectTimeout(timeOutConnection);
            conn.setReadTimeout(timeOutConnection);
            if ( method.equals("POST") ) conn.setDoOutput(true);    // if user use POST
            //檢查Http Response
            if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
                this.httpResponseCode = conn.getResponseCode();
                Log.e("JSON", "Http Response is unsuccessful.");
                Log.e("Number:", Integer.toString(this.getHttpResponseCode()));
                Log.e("Parse Url is =>", url.toString() );
                return null;
            }
            InputStream inputStream = new BufferedInputStream(conn.getInputStream());
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8);
            StringBuilder sb = new StringBuilder();
            String line;
            while ( ( line = bufferedReader.readLine() ) != null )
                sb.append( line +"\n" );
            inputStream.close();
            this.jsonObject = new JSONObject(sb.toString());
        } catch ( SocketTimeoutException te ) {
            this.httpResponseCode = 900;
            Log.e( "getJSONData, Error => ", te.toString() );
            return null;
        } catch ( IOException ie) {
            this.httpResponseCode = 901;
            Log.e( "getJSONDataP,IOError=> ", ie.toString() );
            return null;
        }  catch ( Exception e ) {
            Log.e( "getJSONData, Error => ", e.toString() );
            return null;
        }
        return this.jsonObject;
    }

    // HttpRequest with param
    public JSONObject getJsonData(String action, String method, HashMap<String, String> data) {
        // 檢查網路的狀態
        if ( !netState.checkInternet() ) return null;
        try {
            String url_string = this.hostUrl +"/" +action;
            // if user use GET to make a http request
            if ( method.equals("GET") ) {
                if ( data.size() > 0 ) {
                    int times = 0;
                    for ( Map.Entry<String, String> dataMap : data.entrySet() ) {
                        if (times == 0) {
                            url_string += "?a=a&" +dataMap.getKey() +"=" +dataMap.getValue();
                        }
                        else {
                            url_string += "&" +dataMap.getKey() +"=" +dataMap.getValue();
                        }
                        times++;
                    }
                }
            }
            URL url = new URL(url_string);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setConnectTimeout(timeOutConnection);
            conn.setReadTimeout(timeOutConnection);
            conn.setDoInput(true);
            if ( method.equals("POST") ) {
                conn.setDoOutput(true);    // if user use POST method
                // 寫入資訊
                OutputStream outputStream = conn.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                bufferedWriter.write(setPostParam(data));
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
            }
            //檢查Http Response
            if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
                this.httpResponseCode = conn.getResponseCode();
                Log.e("JSON", "Http Response is unsuccessful.");
                Log.e("Number:", Integer.toString(this.getHttpResponseCode()));
                Log.e("Parse Url is =>", url.toString() );
                //return null;
            }
            InputStream inputStream = new BufferedInputStream(conn.getInputStream());
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8);
            StringBuilder sb = new StringBuilder();
            String line;
            while ( ( line = bufferedReader.readLine() ) != null )
                sb.append(line +"\n");
            inputStream.close();
            this.jsonObject = new JSONObject(sb.toString());
        } catch ( SocketTimeoutException te ) {
            this.httpResponseCode = 900;
            Log.e( "getJSONData, Error => ", te.toString() );
            if (this.jsonObject != null ) Log.e( "JSON: ", this.jsonObject.toString() );
            return null;
        } catch ( IOException ie) {
            this.httpResponseCode = 901;
            Log.e( "getJSONDataP,IOError=> ", ie.toString() );
            if (this.jsonObject != null ) Log.e( "JSON: ", this.jsonObject.toString() );
            return null;
        } catch ( Exception e ) {
            Log.e( "getJSONDataP, Error => ", e.toString() );
            if (this.jsonObject != null ) Log.e( "JSON: ", this.jsonObject.toString() );
            return null;
        }
        return this.jsonObject;
    }

    // 處理使用者 POST 到遠端的東西
    private String setPostParam(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder stringBuilder = new StringBuilder();
        boolean first = true;
        for ( Map.Entry<String, String> dataMap: params.entrySet() ) {
            if ( first )
                first = false;
            else
                stringBuilder.append("&");
            stringBuilder.append(URLEncoder.encode(dataMap.getKey(), "UTF-8"));
            stringBuilder.append("=");
            stringBuilder.append(URLEncoder.encode(dataMap.getValue(), "UTF-8"));
        }
        return stringBuilder.toString();
    }

    // get reponseCode
    public int getHttpResponseCode() {
        return this.httpResponseCode;
    }

}
