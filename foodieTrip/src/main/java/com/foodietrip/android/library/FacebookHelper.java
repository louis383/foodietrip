package com.foodietrip.android.library;

import android.content.Context;

import com.facebook.Session;

public class FacebookHelper {
 
	Context context;
    public FacebookHelper(Context _context) {
    	context = _context;
    }
	
    public boolean isFacebookLogined() {
    	Session session = Session.getActiveSession();
		//Toast.makeText(context, "Facebook Loggin!", Toast.LENGTH_SHORT).show();
		//Toast.makeText(context, "Facebook Logout", Toast.LENGTH_SHORT).show();
		return session != null && session.isOpened();
    }
    
    public void logoutFromFacebook() {
    	Session session = Session.getActiveSession();
    	if (session != null && session.isOpened()) 
    	    session.closeAndClearTokenInformation();
    }
    
    
}
