package com.foodietrip.android.library;

import android.content.Context;

import org.json.JSONObject;

import java.util.HashMap;

public class UserFunctions {
	private JSONParser jsonparser;
	private static String loginURL = "user";
	Context context;
	//Constructor
	public UserFunctions(Context _context) {
		context = _context;
		jsonparser = new JSONParser(_context);
	}
	//Function make Login Request
	//@param email,@param password
	public JSONObject loginUser(String phone,String password) {
		// Buliding Parameters
		String url = loginURL +"/signin";
		HashMap<String, String> params = new HashMap<>();
		params.put("phone", phone);
		params.put("password", password);
		JSONObject json = jsonparser.getJsonData(url, "POST", params);
		//return json
		//Log.e("JSON login = ", json.toString());
		return json;
	}
	//Function make Login Request
	public JSONObject registerUser(String nick_name,String phone,String password, int gender) {
		//Buliding Parameters
		String url = loginURL +"/create";
		HashMap<String, String> params = new HashMap<>();
		params.put("nickname", nick_name);
		params.put("phone", phone);
		params.put("password", password);
		params.put("gender", String.valueOf(gender));
		JSONObject json = jsonparser.getJsonData(url, "POST", params);
		//return json
		//Log.e("JSON register = ", json.toString());
		return json;
	}

	public String getUserName(Context context) {
		DatabaseHandler db = new DatabaseHandler(context);
		HashMap<String, String> user = db.getUserDetails();
		String name = user.get("name");
		return name;
	}

	public String getUserEmail(Context context) {
		DatabaseHandler db = new DatabaseHandler(context);
		HashMap<String, String> user = db.getUserDetails();
		String email = user.get("email");
		return email;
	}

	public int getUserUid(Context context) {
		DatabaseHandler db = new DatabaseHandler(context);
		HashMap<String, String> user = db.getUserDetails();
		int uID = Integer.parseInt(user.get("uID"));
		return uID;
	}

	public String getUserPhone(Context context) {
		DatabaseHandler db = new DatabaseHandler(context);
		HashMap<String, String> user = db.getUserDetails();
		String phone = user.get("phone");
		phone = phone.replace("+886", "0");
		return phone;
	}

	//Function get Login status
	public boolean isUserLoggedIn(Context context) {
		DatabaseHandler db = new DatabaseHandler(context);
		int count = db.getRowCount();
		//Log.e("get row count = ", "" +count);
		return count > 0;
	}
    //Function to logout user
	public boolean logoutUser(Context context) {
		DatabaseHandler db = new DatabaseHandler(context);
		db.resetTable();
		return true;
	}

	public int getResponseCode() {
		int httpResponseCode = jsonparser.getHttpResponseCode();
		return httpResponseCode;
	}

}