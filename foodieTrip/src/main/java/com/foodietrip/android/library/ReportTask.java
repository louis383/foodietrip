package com.foodietrip.android.library;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.util.HashMap;

public class ReportTask extends AsyncTask<String, Integer, Void> {
	Context context;
	String originalValue = "", adviceValue = "", note = "", uID = "", sID = "";
	int reportSwitch, httpReponseCode = 0;
	JSONParser jsonParser;
	public ReportTask(Context _context, int _reportSwitch, String _uID, String _sID) {
		context = _context;
		reportSwitch = _reportSwitch;
		uID = _uID;
		sID = _sID;
		jsonParser = new JSONParser(_context);
	}
	public void addValue(String _originalValue, String _adviceValue, String _note) {
		originalValue = _originalValue;
		adviceValue = _adviceValue;
		note = _note;	
	}
	public void addPhotoUrl(String _originalPhotoUrl) {
		originalValue = _originalPhotoUrl;
	}
	public void addMessage(String _originalMessage) {
		originalValue = _originalMessage;
	}
	@Override
	protected Void doInBackground(String... message) {
		String url = "report/submit";
		String reportItemString = String.valueOf(reportSwitch);
		String adviceItemString = adviceValue;    //建議值
		String noteString = note;    //其他建議
		HashMap<String, String> params = new HashMap<>();
		params.put("uID", uID);
		params.put("id", sID);
		params.put("report_item", reportItemString);
		params.put("origin_thing", originalValue);
		params.put("correct_thing", adviceItemString);
		params.put("remark", noteString);
		JSONObject json = jsonParser.getJsonData(url, "POST", params);
		if (json == null) {
			Log.e("json is null => ", "Report");
			httpReponseCode = jsonParser.getHttpResponseCode();
			return null;
		}
		httpReponseCode = jsonParser.getHttpResponseCode();
		//Log.e("Report json Message =>", json.toString());
		return null;
	}
	
	public int getHttpResponseCode() {
		return httpReponseCode;
	}
}
